nextflow.enable.dsl=2

params.reads = false
params.reference = false
params.output_dir = false
params.coverage_threshold = 5

include { INDEX_REFERENCE; ASSESS_COVERAGE; COMBINE_RESFINDER_RESULTS } from './modules/coverage_assessment.nf'

if (params.reads && params.reference && params.output_dir ){
  workflow {
    reads = Channel
    .fromFilePairs(params.reads)
    .ifEmpty { error "Cannot find any reads matching: ${params.reads}" }
    
    INDEX_REFERENCE(file(params.reference))
    ASSESS_COVERAGE(reads, INDEX_REFERENCE.out, params.coverage_threshold)
    COMBINE_COVERAGE_RESULTS(ASSESS_COVERAGE.out.collect())
  }
} else {
    error "Please specify reads, a reference and an output directory with --reads, --reference and --output_dir"
}
