params.output_dir = false

process INDEX_REFERENCE {
  tag "prepare reference"

  input:
    file(reference)
  output:
    path("index")

  script:
    """
    mkdir index
    bwa index ${reference} -p index/reference
    """

}

process ASSESS_COVERAGE {
  tag "$id"

  input:
    tuple(val(id), path(reads))
    path(reference_index_dir)
    val(depth_threshold)
  output:
    path("${id}.coverage.txt")
  script:
    """
    # map reads
    bwa mem -a ${reference_index_dir}/reference ${reads[0]} ${reads[1]} \
    | samtools view -bS -F 4 - \
    | samtools sort -O bam -o ${id}.sorted.bam

    # get length of reference
    REF_LEN=\$(samtools view -H ${id}.sorted.bam | sed -n 's/.*LN:\\(.*\\)/\\1/p')
    # get number of positions where coverage is greater than 5
    COVERED_POSITIONS=\$( bedtools genomecov -ibam ${id}.sorted.bam -d  |  awk -F"\t" '\$3>${depth_threshold}{print \$2}' | wc -l )
    PERCENT_COVERAGE=\$(echo "scale=2;100*\${COVERED_POSITIONS}/\${REF_LEN}" | bc)
    echo "${id}\t\$PERCENT_COVERAGE" > ${id}.coverage.txt
    """
}

process COMBINE_COVERAGE_RESULTS {
    publishDir params.output_dir, mode: 'copy'
    input:
      path(result_files)

    output:
      path("combined_results.tsv")

    script:
      """
        
        for file in ${result_files}; do
          cat \${file} >> combined_results.unsorted.tsv
        done

        echo "Sample\tPercent coverage" > combined_results.tsv
        cat combined_results.unsorted.tsv | sort -nrk 2  >> combined_results.tsv
      """
}


