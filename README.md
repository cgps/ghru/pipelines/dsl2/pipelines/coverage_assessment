## Instructions
#### Install requirements
1. Nextflow
2. Docker
#### Get workflow and Docker container
```
git clone https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/coverage_assessment.git
docker pull bioinformant/coverage_assessment:latest
```
#### Running the pipeline
Example here for a plasmid `p13ARS_MMH0112-3.fas` as reference
```
nextflow run main.nf --reads "/path/to/fastqs/*{R,_}{1,2}.fastq.gz" --reference p13ARS_MMH0112-3.fas --output_dir coverage_output -resume
```
Optionally the `--coverage_threshold` (the minimum depth of coverage for a postion to be considered as covered) can be specified (default is 5)